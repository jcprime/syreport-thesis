\section{Methodology}
\label{methodology}

%\oiface{GULP FIRST, WITH DEHYDRATE, AND THEN CP2K (HOPEFULLY WITH DEHYDRATE ALSO)}

The procedure followed so far in this investigation is based on increasing levels of complexity in the models used: first using interatomic potential models to reproduce existing computational predictions in GULP, with the use of \texttt{dehydrate} to automate parts of the process, then using electronic structure models in CP2K, with the aim to improve upon the results obtained using GULP.

\subsection{Interatomic potential models using GULP}
\label{ip_gulp}

%Details on GULP calculations in general [][#French2011] [][#Gibbs2004] [][#Lewis2002] [][#White2004]

The General Utility Lattice Program (GULP)~\cite{Gale2003} is a software package designed to remove the manual aspect of interatomic force calculations.  It has a wide range of functions and potential material applications; purely covalent systems to purely ionic ones, minuscule clusters to condensed bulk crystals, and various levels of computational complexity can be included or excluded at the user's design.

GULP constructs a series of interatomic potentials, given the input keywords and parameters, to represent the given system, and uses the sum over all interatomic interactions to guide the overall optimisation process.  The aim of the global/structural optimisation is one of energy minimisation; that is, the most stable (optimal) structure is that which is overall lowest in energy.  This method is most effective, in general terms, for the more well-behaved, (primarily) ionic systems, since anomalous or electron-sharing behaviours are inevitably less well-represented by interatomic potentials of this sort.

For example, if an input file specifies the inclusion of a Buckingham potential for a given pair of atom types, as shown below:

\begin{verbatim}
buckingham
Si core Al core A rho C minr maxr
\end{verbatim}

Then GULP will impose interactions between the given atoms (silicon and aluminium in this case), between the given atomic separations (\texttt{minr} and \texttt{maxr} in this case), wherein the potential is given by $E_{Buck} = A \exp^{\left(-\frac{r}{\rho}\right)} - \frac{C}{r^{6}}$, for all atoms of those kinds in the given \texttt{fractional} section.

% FOR REFERENCE: buckingham <inter/intra> <bond/x12/x13/mol/o14> <kcal/kjmol> <ener/grad> <scale14> <type_of_bond> atom1 atom2 A rho C <rmin> rmax <3*flags>

The system's structure is then locally optimised, by default using the Broyden-Fletcher-Goldfarb-Shanno (BFGS) algorithm~\cite{Shanno1970}, to the nearest local minimum; the rational functional optimisation (RFO) method~\cite{Press1993} is used to locate stationary points on the potential energy surface of the given system.

% LOCAL OPTIMISATION (see https://nanochemistry.curtin.edu.au/local/docs/gulp/gulp4.2_manual.pdf, page 42)
% Expand internal energy as a Taylor series
% First derivatives collectively written as gradient vector $g$
% Second derivative matrix referred to as Hessian matrix $H$

\subsubsection{Modelling the natrolite family}
\label{gulp_natrolites}

Calculations were undertaken with the aim to reproduce previous results obtained for the dehydration of natrolite and scolecite~\cite{Gibbs2004}.  Previous investigations had also partially simulated the dehydration of mesolite, but this investigation so far has both reproduced said results and continued the calculations to complete the simulation of mesolite's full dehydration pathway.

The unit cell coordinates for natrolite, scolecite, and mesolite were taken from experimentally-obtained results~\cite{Stuckenschmidt1993,Stuckenschmidt1997,Stuckenschmidt2000}.  Starting from the fully-hydrated structures, the water molecules were removed in successive steps, one at a time, and at each stage the full set of possible water-removals were simulated to find the lowest in energy of the final optimised structures.

\begin{table}[ht]
\caption{Buckingham parameters used in GULP to model the natrolite family}
\label{tab:ip_param_buckingham}
\begin{tabular}[c]{|l|l|l|l|l|l|l|}
\hline
Atom 1 & Atom 2 & A/eV & $\rho$ /\r{A} & C/eV \r{A}$^{6}$ & Min Cutoff/\r{A} & Max Cutoff/\r{A} \\
\hline
Si core & O2 shel & 1283.9070 & 0.320520 & 10.662 & 0.000 & 20.000 \\
Al core & O2 shel & 1460.3000 & 0.299120 & 0.0000 & 0.000 & 20.000 \\
O2 shel & O2 shel & 22764.000 & 0.149000 & 27.880 & 0.000 & 20.000 \\
Na core & O2 shel & 1226.8400 & 0.306500 & 0.0000 & 0.000 & 20.000 \\
Ca core & O2 shel & 1090.4000 & 0.343700 & 0.0000 & 0.000 & 16.000 \\
Li core & O2 shel & 1303.9000 & 0.260000 & 0.0000 & 0.000 & 20.000 \\
K core  & O2 shel & 1000.3000 & 0.361980 & 10.569 & 0.000 & 20.000 \\
Mg core & O2 shel & 1428.5000 & 0.294530 & 0.0000 & 0.000 & 20.000 \\
Al core & O1 shel & 1460.3000 & 0.299120 & 0.0000 & 0.000 & 16.000 \\
Na core & O1 shel & 450.49800 & 0.306500 & 0.0000 & 0.000 & 20.000 \\
Li core & O1 shel & 1303.9000 & 0.260000 & 0.0000 & 0.000 & 20.000 \\
K core  & O1 shel & 1000.3000 & 0.361980 & 10.569 & 0.000 & 20.000 \\
Ca core & O1 shel & 435.07000 & 0.343700 & 0.0000 & 0.000 & 12.000 \\
Si core & O1 shel & 1283.9070 & 0.320520 & 10.662 & 0.000 & 16.000 \\
O1 shel & O2 shel & 22764.300 & 0.149000 & 28.920 & 0.000 & 16.000 \\
H core  & O1 shel & 396.27000 & 0.250000 & 10.000 & 0.000 & 20.000 \\ % (inter)
H core  & O2 shel & 396.27000 & 0.250000 & 0.0000 & 0.000 & 10.000 \\
\hline
\end{tabular}
\end{table}
\begin{table}[ht]
\caption{Morse parameters used in GULP to model the natrolite family}
\label{tab:ip_param_morse}
\begin{tabular}[c]{|l|l|l|l|l|p{2cm}|l|l|}
\hline
Atom 1 & Atom 2 & D$_{e}$/eV & a/\r{A}$^{-1}$ & R$_{0}$/\r{A} & Coulomb subtraction & Min Cutoff/\r{A} & Max Cutoff/\r{A} \\
\hline
H core & O1 shel & 6.2037100 & 2.2200 & 0.92376 & 0.5000 & 0.000 & 1.100 \\
H core & H core & 0.0000000 & 2.8405 & 1.50000 & 0.5000 & 0.000 & 1.650 \\
\hline
\end{tabular}
\end{table}
\begin{table}[ht]
\caption{Lennard 12-6 parameters used in GULP to model the natrolite family}
\label{tab:ip_param_lj}
\begin{tabular}[c]{|l|l|l|l|l|l|}
\hline
Atom 1 & Atom 2 & A/eV \r{A}$^{12}$ & B/eV \r{A}$^{6}$ & Min Cutoff/\r{A} & Max Cutoff/\r{A} \\
\hline
O1 shel & O1 shel & 39344.980 & 42.150000 & 0.000 & 20.000 \\
\hline
\end{tabular}
\end{table}
\begin{table}[ht]
\caption{Three-body parameters used in GULP to model the natrolite family}
\label{tab:ip_param_three}
\begin{tabular}[c]{|l|l|l|p{1.2cm}|l|p{2cm}|p{2cm}|p{2cm}|}
\hline
Atom 1 & Atom 2 & Atom 3 & k/eV $\cdot$ rad$^{-2}$ & $\theta_{0}$ /\textdegree & Max Cutoff$_{1-2}$/\r{A} & Max Cutoff$_{1-3}$ /\r{A} & Max Cutoff$_{2-3}$ /\r{A} \\
\hline
Al core & O2 shel & O2 shel & 2.0972 & 109.47000 & 1.900 & 1.900 & 3.500 \\
Si core & O2 shel & O2 shel & 2.0972 & 109.47000 & 1.900 & 1.900 & 4.000 \\
O1 shel & H core & H core & 4.1998 & 108.69000 & 1.500 & 1.500 & 2.000 \\ % Intra
\hline
\end{tabular}
\end{table}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
% Just for my own reference: keywords `opti [eigen inten] conp molq rfo [nosym full phonon comp]`, where those in square brackets were used in natrolite input files but not the others

The interatomic potential parameters were specified as shown in \textbf{Tables \ref{tab:ip_param_buckingham}, \ref{tab:ip_param_morse}, \ref{tab:ip_param_lj}, and \ref{tab:ip_param_three}}, wherein the intra-framework and framework-EFC interactions were provided by~\citet{Sanders1984}, using formal charge potentials, the water-specific interactions were provided by~\citet{deLeeuw1998,deLeeuw1999}, and the (framework) oxygen-water interactions were represented by a modified version of the water-specific potentials~\cite{Lewis2002}.  The given water-specific potentials were used specifically because they were originally derived for modelling solvation at ionic surfaces, so their use in this case to model hydrated cations is reasonable.

The overall longer-range interactions were calculated in GULP using the Ewald summation method, while the short-range interactions were calculated up to cutoff distances between 10 and 20 \r{A}.  The rational function optimisation (RFO) method~\cite{Simons1983,Banerjee1985} was used to converge the overall energy of the system to a minimum, at constant pressure.

\oiface{NEEDS UN-BULLET-ING BELOW!}

\begin{itemize}
    \item Perform energy minimisation (RFO method used to converge the energy to a minimum) at constant pressure
    \begin{itemize}
        \item Using an Ewald summation for the longer-range electrostatic interactions
        \item Cutoffs between 10--20 \r{A} for the short-range interactions
        \begin{itemize}
            \item Formal charge potentials used for intra-framework interactions and framework-EFC interactions~\cite{Sanders1984}
            \item Water interactions were taken from~\citet{deLeeuw1998} and~\citet{deLeeuw1999}; applicable because they were originally derived for modelling solvation at ionic surfaces, and so using them for modelling hydrated cations is not too much of a stretch{\ldots}
            \item Use modification for water-O(FW) interactions as given in~\citet{Lewis2002}, which adapts the existing potential parameters, e.g.\ framework oxygen has effective charge of $2^{-}$, whereas water oxygen would have a reduced effective charge ($1.4^{-}$ ish, if I remember rightly?)
        \end{itemize}
    \end{itemize}
\end{itemize}

\subsection{\texttt{Dehydrate}~\cite{White2004}}
\label{dehydrate}

%\oiface{EXPLAIN WHAT DEHYDRATE IS AND WHY WE'RE USING IT}

\texttt{dehydrate} is an intermediary between GULP and the systematic dehydration processing that would otherwise require the user to manually analyse and edit potentially upwards of 100 input files.  It is specifically designed for dehydration investigations, as it counts the number of water-component atoms in the input file passed to it on the command line, calculates which atoms compose which water molecule, and counts the number of water molecules, before processing the input file into several output files as outlined below.  No functionality exists in \texttt{dehydrate} as yet to process any other molecule in a similar way, which is an important criterion for extension in the near future.

The current \texttt{dehydrate} is written in C, and a new version is currently in the very early stages of development.  The mechanism of action for using GULP with \texttt{dehydrate}, starting with a unit cell structure containing \texttt{n} water molecules, is as follows:

\begin{enumerate}
    \itemsep0em
    \item Take the \texttt{.dump} output file of the lowest-energy configuration at the given stage of dehydration as being the most stable structure, and copy e.g. \texttt{zeolite\_n.dump} to \texttt{zeolite\_n-1.gin}, where \texttt{n} is the number of water molecules in the current (optimised) structure
    \item Pass \texttt{zeolite\_n-1.gin} to \texttt{dehydrate}, which:
    \begin{itemize}
        \itemsep0em
        \item Reads in and stores all lines preceding coordinate lines (the start of which is indicated by the \texttt{fractional} signifier)
        \item Reads in coordinate lines, ignoring any lines beginning with \texttt{\#} (those lines that are commented out)
        \item Identifies the atoms making up the water molecules (and counts number of water molecules, water-constituent atoms, and total atoms), and which atoms are connected to which (calculated to be those atoms within bonding distance of the atom under current consideration)
        \item Reads in and stores all lines following the end of the coordinates
        \item \textbf{For every water molecule detected in the previous step, in turn:} Writes out a numbered input file, with everything identical to the original input, with the given water molecule commented out
    \end{itemize}
    \item Run all the newly-written \texttt{.gin} files through GULP, compare output energies with \texttt{grep \textquotesingle Final ene\textquotesingle~*.gout | sort -n -k5}, and the top file in the resultant list will be the most likely configuration at that stage in the dehydration % NB: \TEXTQUOTESINGLE REQUIRES TEXTCOMP PACKAGE TO BE INCLUDED!
    \begin{itemize}
        \itemsep0em
        \item Be sure to check that the top file in the list isn't enormously lower in energy than the rest of the list, or else you may have an unconverged structure
    \end{itemize}
    \item Repeat the above steps until there are no more water molecules remaining, at which point the (un-optimised) dehydrated zeolite structure should have been obtained
\end{enumerate}

\subsubsection{Extending \texttt{dehydrate}}
\label{extending_dehydrate}

The version of \texttt{dehydrate} that is currently in development aims to be modular in design, wherein overlap of various computational-chemistry codes could be overseen by \texttt{dehydrate} at the user's preference, and more molecules than water could be considered and manipulated in the input files.

Ideally, \texttt{dehydrate} should eventually be able to manage the following three situations, to start:

\begin{itemize}
    \itemsep0em
    \item Taking the GULP input files and outputting new input files systematically using the same format, to be reused in GULP
    \item Taking the existing GULP input files previously output by the original version of \texttt{dehydrate}, extracting the coordinates, converting them to a CP2K-readable format, creating CP2K input files for each GULP file being part-converted, and inserting the path to the new coordinate file into the relevant CP2K input file
    \item Taking a CP2K input file and going through the same process as for GULP, but with the intention of creating the new input files from an existing CP2K-optimised input file as a starting point
\end{itemize}

This is the current stage of this branch of the project at the time of writing.

\subsection{CP2K~\cite{CP2K}}
\label{cp2k}

The next aim of this investigation was to apply a similar optimisation process to the same materials, using an electronic-structure approach, in order to find out how well the electronic-structure-based calculations fare in comparison to the original interatomic-potential models, both in terms of speed and accuracy.

This stage of the investigation involved using Quickstep~\cite{VandeVondele2005}, part of the CP2K software package, to model the natrolite family using the existing GULP input files --- starting with the fully-hydrated structures, taken directly from experimental findings --- as a starting point.  Quickstep uses a Gaussian-planewave approach~\cite{VandeVondele2005} to model the atomic interactions, rather than the more simple fitted potential functions used in GULP.

Since the purpose of this early stage in testing was to find an optimal intermediate model using CP2K, where the output results concur with those obtained previously and with experiment, but that require the least amount of computational ``sacrifice'' to obtain such results, ``worse'' parameters --- basis sets, pseudopotentials, functionals --- were used to begin with.  These are being improved systematically at each stage, with the intention to find the combination of the least computationally-intensive parameters that could reasonably reproduce experimental findings.

% Computational details for CP2K models

% NB: PADE = LDA

At the time of writing, the hydrated natrolite family are being tested using:
\begin{itemize}
    \itemsep0em
    \item ``DZVP-MOLOPT-SR-GTH'' basis sets~\cite{Goedecker1996,Guidon2010}; the requirement for the basis set reduction and modification to one including ``MOLOPT'' became necessary due to a persistent Cholesky decomposition error produced upon use of higher basis sets
    \item ``GTH-PADE'' pseudopotentials~\cite{Goedecker1996}
    \item PADE exchange-correlation functional
    \item Self-consistent field (SCF) grid cutoff at 750 \r{A}
    \item Relative cutoff at 90 \r{A}, as deduced from cutoff tests
    \item The orbital-transformation (OT) method~\cite{VandeVondele2003}, in tandem with conjugate gradient minimisation techniques~\cite{Press1993} to optimise the configuration at every iteration of the SCF loops
\end{itemize}

An additional obstacle presented itself alongside that requiring a lower basis set, requiring the removal of any preconditioner during initial simulation; this can be attributed to the use of experimentally-determined coordinates, and thus providing a ``pre-optimised'' structure for optimisation. The self-consistent field calculations begin using a random guess, using nested loops to aid optimisation.

% Cutoff tests
\subsubsection{Cutoff tests}
\label{cutoff_tests}

\oiface{WORDS HERE PLEASE!}

\subsubsection{Notes on coordinates in CP2K vs. GULP}
\label{coordinates}

Using the coordinates provided in the existing GULP input files, converted into \texttt{xyz} format using a simple bash script, the atoms were defined as would be expected --- by their chemical formulae --- excepting ``O1'', denoting a water-oxygen atom, and ``O2'', denoting a framework oxygen atom.

% Specific notes to bear in mind when developing the new version of dehydrate

CP2K can take coordinate input in many forms, which allows for greater scope in terms of routes in developing a generalised version of the existing, GULP-specific \texttt{dehydrate}, to perform the same essential actions on any type of input (coordinate) file with which it is provided.

CP2K also allows the user to explicitly define atom labels for specific atom types, which reduces the need for \texttt{dehydrate} to add additional features specific to the handling of multiple atom labels; this is, however, still likely to be needed for the GULP-specific functionality.

% RANDOM NOTES

% CP2K can also take separate coordinate files or input files with the coordinate section included directly; the former option would likely be the more flexible

% We also want to find out whether the quantum-mechanical model actually produces output that is measurably more accurate than that produced by interatomic potential models using GULP

% Paying particular attention to thermodynamic calculations
