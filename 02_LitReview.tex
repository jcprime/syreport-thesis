\section{Literature Review}
\label{literature_review}

\subsection{Zeolite classification}
\label{zeolite_classification}
The classification of a zeolite depends on several factors: framework density, space group, cell parameters, and even idealised chemical formulae.
%The idealised chemical formula, space group, and cell parameters can be useful, \emph{except they may not match with the main topology of their overall grouping, so should be considered carefully!} The framework density gives the number of T atoms per 1000  \r{A}$^{3}$ , and should be expected to reside between 12 and 20 in general for inflexible-framework zeolites~\cite[p.~10]{Baerlocher2001_Atlas}.  

However, zeolites are primarily classified based on their framework topology.  This is a set of unique combinations of structural properties, such as their coordination sequence and vertex symbol~\cite[p.~5]{Baerlocher2001_Atlas}, which govern the way in which the aluminosilicate frameworks are composed.  For example, given that zeolite frameworks consist of \ce{TO_{4}} tetrahedra, where T is either Si or Al, and the oxygens are shared ``bridges'' between adjacent tetrahedra, then the coordination sequence indicates the ``spread'' of neighbouring O-bridged T atoms from a given starting T atom.  It can be calculated using $N_{k} \le 4 \cdot 3^{k-1}$, where $N_{k}$ is the $k$-th ``sphere'' of coordination ($N_{0} = 1$).  Similarly, the corresponding vertex symbol highlights the smallest ring size for each pair of opposite angles in the tetrahedron surrounding the starting T atom.  The combination of these two components alone is enough to distinguish all known zeolite framework types~\cite[p.~5]{Baerlocher2001_Atlas}.

Zeolites are also composed of secondary building units (SBUs), which are topological constructs designed to aid in their classification --- where their primary building units are the \ce{TO_{4}} tetrahedra.  All SBUs have distinct identifying symbols, are non-chiral, contain up to 16 T atoms, and are present in unit cells in whole numbers~\cite[p.~6-7]{Baerlocher2001_Atlas}.

% Structural details

\subsection{The natrolite family}
\label{natrolite_family_structure}

\subsubsection{Natrolite structure}
\label{natrolite_structure}

Natrolite~\cite{Pauling1930,Meier1960} has a framework structure given by the NAT topology~\cite[p.~204]{Baerlocher2001_Atlas}, wherein its idealised cell constants give a tetragonal unit cell and orthorhombic symmetry~\cite{Meier1960}, composed of c-axis-linked 4=1 building units, and an ordered distribution of Al and Si, with an empirical formula given by \ce{Na_{2}Al_{2}Si_{3}O_{10} $\cdot$ 2 H_{2}O}, and a full unit cell given by \ce{Na_{16}[Al_{16}Si_{24}O_{80}]$\cdot$ 16 H_{2}O}.  The 4=1 SBU is itself made up of \ce{T_{5}O_{10}}, and when linked along natrolite's c-axis, it produces the framework ``chains''~\cite{Lee2001}, which themselves orientate to form helical channels.

Sodium is the primary extra-framework cation, and natrolite's natural form is hydrated.  Ordinarily, solvated sodium ions would be surrounded by six water molecules~\cite{Rempe2001}, but in the confines of natrolite's pores, the strictly-charged framework oxygens can replace some of the water molecules; this may introduce some degree of structural distortion around the cations, and sodium usually still requires two water molecules to complement the framework oxygens~\cite{Meier1960}.  The cationic charge requires the oxygens to orientate towards them, and so the stucture will distort further, to accommodate the necessary ``shaping'' of the framework atoms and the water molecules around the cations in its pores; the sodium cations prefer to orientate nearer the walls of the channels, whereas the water molecules prefer to be positioned in the larger spaces within them~\cite{Meier1960,Artioli1984}.

The experimentally-derived starting structure used in the model for natrolite in this investigation was taken from~\citet{Stuckenschmidt1993}.

\subsubsection{Scolecite structure}
\label{scolecite_structure}

Scolecite~\cite{Taylor1933,Falth1979} has an empirical formula given by \ce{CaAl_{2}Si_{3}O_{10}$\cdot$ 3 H_{2}O}, and a full unit cell given by \ce{Ca_{8}[Al_{16}Si_{24}O_{80}]$\cdot$ 24 H_{2}O}, and the same NAT topology as natrolite, but with calcium as the primary extra-framework cation; since calcium ions are twice as charged as sodium ions, half the number of calcium ions are required to compensate for the Al-Si substitution in scolecite than of sodium in natrolite~\cite{Falth1979,Kvick1985}.  Understandably, this in itself introduces a further level of distortion than is present in natrolite~\cite{Falth1979}, and thus its symmetry is lowered to monoclinic~\cite{Kvick1985}.  Calcium ions are also ordinarily hydrated, surrounded by seven water molecules~\cite{Megyes2004,Zavitsas2005}; in scolecite's pores, three of these are retained, on average, with four framework oxygens completing the charge compensation~\cite{Falth1979}.

% \oiface{Need to find Alberti1982 (ALBERTI, A., PONGILUPPI, D. & VEZZALINI, G. (1982). Neues Jahrb. Mineral. Abh. 143, 231-248) to cite alongside Kvick1985 for monoclinic scolecite symmetry.}

The experimentally-derived starting structure used in the model for scolecite in this investigation was taken from~\citet{Stuckenschmidt1997}.

\subsubsection{Mesolite structure}
\label{mesolite_structure}

Mesolite~\cite{Artioli1986} is also based on the NAT topology, but its generally-accepted structure is not as intuitive.  With an empirical formula given by \ce{Na_{2}Ca_{2}(Al_{2}Si_{3}O_{10})_{3}$\cdot$ 8 H_{2}O}, and a full unit cell formula given by \ce{Na_{16}Ca_{16}(Al_{16}Si_{24}O_{80})_{3}$\cdot$ 64 H_{2}O}, it was named ``meso-'' (``middle'') for its ``intermediate'' nature between the structures of natrolite and scolecite.

It has been postulated that mesolite is composed of ordered layers, alternating one sodium-containing layer and two calcium-containing layers, based on X-ray diffraction results~\cite{Artioli1986}.  The alternation of cations from layer to layer introduces a distortion to the framework, lowering its overall symmetry compared to that of natrolite and scolecite, twisting its chains and shifting the positions of its extra-framework cations and water molecules~\cite{Artioli1986}.

% Hence the need for computational modelling

It is not particularly easy to use experimental techniques to confirm or disprove theoretical structures for such (comparatively) dense microporous materials.  This makes computational modelling an especially viable tool to compensate for the lack of physical knowledge about these specific structures.

An appropriate model is therefore required that has the best combination of computational efficiency and scientific accuracy, which is what this project is aiming to investigate; the experimentally-derived starting structure used in the model for mesolite was taken from~\citet{Stuckenschmidt2000}.

\oiface{\subsubsection{Paranatrolite}}
\oiface{\label{paranatrolite}}

\oiface{This is a half-hydrated natrolite variant.}

\oiface{\subsubsection{Substituted NAT variants}}
\oiface{\label{experiment_substituted_natrolites}}

\oiface{K-GaSi-NAT is most likely to be of industrial use, as it undergoes a pressure-induced volume expansion that is irreversible upon pressure release.  It also undergoes a crystallographic rearrangement that produces H-bonded water nanochains surrounded by potassium cations.}

\oiface{Na-AlSi-NAT undergoes a reversible volume expansion under pressure.  It also displays the opposite crystallographic rearrangement, namely that H-bonded water nanotubes surrounding sodium cations.}

\oiface{Aluminogermanate-NAT~\cite{Tripathi2000}, gallosilicate-NAT~\cite{Xie1998}, potassium-gallosilicate~\cite{????}}

\subsection{Experimental observations}
\label{experimental_observations}

\subsubsection{Hydration and dehydration}
\label{hydration_and_dehydration}

Natrolite has been found to undergo a reversible and ``linear'', ``pairwise'' dehydration process, in that the overall change in dehydration enthalpy for every successive water molecule removed is approximately the same as those preceding it~\cite{Gibbs2004}.  The ``pairing'' of water molecules within the pores has been shown to influence the order of dehydration, wherein one cation will lose a water molecule, and the next water molecule to be removed will be coordinated to a different cation but within the same pore as the preceding one~\cite{Gibbs2004}.  Each initial loss of water from a given pore will make the loss of a further molecule from the same pore more favourable by way of destabilisation~\cite{Gibbs2004}.  It has been suggested that the larger number of (sodium) cations in natrolite, compared to the number of (calcium) cations in scolecite, enable more effective rearrangement to stabilise the surrounding framework within each pore, by reorientating themselves towards the framework and thus minimise its distortion~\cite{Gibbs2004}.

Scolecite, in contrast, undergoes two separate stages of dehydration upon heating~\cite{Gottardi1985}; the removal of the first eight water molecules, each from the ``OW(2)'' sites~\cite{Stahl1994_XRD_ScolMes}, losing a water molecule from each calcium ion, layer by layer within the unit cell~\cite{Stahl1994_XRD_ScolMes}.  The observation of a small energetic distinction between the first four water molecules lost and the four following them has also been made, wherein the calcium cations shift in position to increase their coordination~\cite{Stahl1994_XRD_ScolMes,Gibbs2004}.  The reasoning behind scolecite's specific dehydration profile is that the removal of the first water molecule in each layer destabilises the remaining water molecules in the layer, thus making them easier to remove~\cite{Gibbs2004}.  The remaining 16 water molecules are lost at much higher temperatures, approaching 400\textdegree C~\cite{vanReeuwijk1974}, each with $\Delta \mathrm{H}_{dehyd}$ greater than those lost in the first stage of dehydration, indicating an increased overall destabilisation during this higher-temperature second dehydration step~\cite{Gibbs2004}.  Scolecite also displays temperature-induced amorphisation, once it has been dehydrated completely~\cite{Lee2002_PIExpansion}.

Mesolite undergoes a still more complex dehydration process, thanks to its composition of both natrolite-like and scolecite-like layers, which takes place in three stages: first, the calcium-coordinated water molecules are removed, then the cations rearrange to lower their overall energy, and then the structure undergoes full amorphisation of its framework~\cite{vanReeuwijk1974,Stahl1994_XRD_ScolMes,Stahl1994_XRD_Mes,Prasad2007}

\subsubsection{Pressure and temperature effects}
\label{experiment_pressure_temperature}
Many zeolites display some interesting properties when exposed to environmental extremes, such as pressure and temperature; the most pertinent, for this study, are pressure-induced hydration~\cite{Graham2001,Lee2001,Baur2003} and negative thermal expansion~\cite{Lightfoot2001}.  However, much more is known about temperature-induced phase transitions than pressure-induced ones~\cite{Lee2001}.

\oiface{PIH - early physical characterisation~\cite{Moroz2001} and initial diffraction experiments~\cite{Hazen1983}}

Pressure-induced expansion (PIE) is a phenomenon wherein certain microporous materials expand in volume with increasing pressure, and pressure-induced hydration (PIH) is the specific variant in which such materials take in water as the pressure being exerted on them increases, as a result of the ``empty'' space contained within their frameworks~\cite{Graham2001,Baur2003}.  Such materials, when under high pressures in water-rich environments, are referred to as ``over-'' or ``super-hydrated''.  Natrolite, for example, doubles its intake of water molecules between 0.8 and 1.2GPa~\cite{Lee2001,Lee2002_PIExpansion}; it was the first zeolite experimentally-observed to undergo PIH~\cite{Belitsky1992,Lee2001}, while simultaneously increasing in cell volume.  Several materials have since been shown to undergo PIH: large-pore zeolites~\cite{Colligan2004}, clays~\cite{You2013}, and graphite oxide~\cite{Talyzin2008}.

Negative thermal expansion (NTE) is effectively the counterintuitive contraction of a material upon heating~\cite{Liu2011}.  Ordinarily, increasing the temperature of a material will induce an expansion in its volume; however, certain materials, under high temperatures, are reduced in volume.  Many materials that undergo this process are microporous~\cite{Lightfoot2001}, which facilitates the process because their structures can rearrange to accommodate the contraction without losing so much stability that a structural collapse into its fully-dense solid form is more favourable.

\oiface{\subsubsection{Other zeolites}}
\oiface{\label{experiment_other_zeolites}}

\oiface{ZSM-5 (from one of the blatherings in PostgradStuff.scriv)~\cite{Ruiz-Salvador2013}
Al-Si substitution in ZSM-5 (with Si/Al = 47) occurs preferentially at the T14 site in particular, as given by IP-model simulations.  The global minimum configuration depends on the interplay of the energetics of the substitution at specific sites and the Al-Al interactions (which prefer to maximise the distance between Al atoms - Dempsey's rule - because of Coulombic repulsion).}

\oiface{The interactions between Al atoms is not solely dependent on the distance between them.  This is evidenced by the interaction energy appearing to be inversely proportional to the square of the Al-Al distance, rather than the inverse-linear proportionality expected for charge repulsion alone.}

\oiface{The anisotropy of the framework density also influences the magnitude of the interactions, as well as the Al-Al distances.}

\subsection{Modelling techniques}
\label{modelling_techniques}

Interatomic potential (IP) models make use of functions of a system's nuclear positions, which together provide an overall energy for the system~\cite{Catlow_CMoMM_BOOK}.  Several types of simulation can make use of these models, each providing a different focus for their calculations.  For example, Monte Carlo simulations~\cite{Hastings1970,Kroese2014}, using probability distributions and the Metropolis algorithm~\cite{Metropolis1953,Liu2000,Martino2013}, can give details about ensemble averages~\cite{Catlow_CMoMM_BOOK}, whereas molecular dynamics simulations~\cite{Fermi1955}, using equations to incorporate motion into their calculations, can give details on the specific mechanisms involved in reaching a given outcome, such as a final energy-minimised structure~\cite{Catlow_CMoMM_BOOK}.  In this investigation, however, the initial focus will require use of the simpler minimisation methods, as the primary interest at this point is final energies and ground-state configurations at each stage in (de)hydration.

A more accurate set of ``electronic structure'' methods, such as density functional theory-based methods, employ techniques designed to solve the Schr\"{o}dinger equation, In its most general form, the Schr\"{o}dinger equation describes the state of a system in terms of wavefunctions $\psi$: \begin{equation}E\psi = \hat{H}\psi,\end{equation}where $\hat{H}$ is the Hamiltonian operator, which is dependent on the system in question.  These wavefunctions can form standing waves, corresponding to atomic (or molecular) orbitals $\phi_{i}$.

Essentially, by attempting to solve this equation for the many-body problem, using various approximations, electronic structure (ES) techniques aim to provide more accurate structural predictions than can be obtained from interatomic potentials alone.

% IP models have been used with some success, for natrolites and others

\subsubsection{Interatomic potentials and the natrolite family}
\label{interatomic_potentials_natrolites}

The natrolite family have been modelled quite successfully using interatomic-potential models~\cite{Gibbs2004}.  However, there has been one consistent problem with these models, and that is in the accuracy of thermodynamic quantities for some members, when compared with experiment.

\oiface{Thermodynamic observations of natrolite and scolecite~\cite{Gibbs2004}.}

% Limitations of IP models for natrolite family so far
\oiface{LIMITATIONS OF IP MODELS FOR NATROLITE FAMILY SO FAR}

% SOMETHING ABOUT WATER EXPLODING IN GULP, SO ONLY DEHYDRATION SIMULATIONS ARE POSSIBLE
\oiface{EXPLODING WATER-SQUISHING ENDEAVOURS IN GULP MEAN ONLY DEHYDRATION SIMULATIONS CAN BE DONE WITH GULP}

Additionally, using interatomic potential models to simulate the process of hydration is rather difficult, since hydration of an anhydrous zeolite involves the removal of a hydrated extra-framework cation from its aqueous surroundings and ``pushing'' it --- along with as many water molecules whose coordination it can retain throughout --- into the confines of the zeolitic pores.  The structural optimisation algorithms used with such simulations are rarely robust enough to complete the hydration process without the structure breaking apart.

More robust models will therefore be needed in order to successfully model the hydration of the natrolite family, while the simpler interatomic potential models are sufficient for the dehydration of natrolite and scolecite.  Mesolite's dehydration process is somewhat more complex, and is proving difficult to model using the same parameters as used with natrolite and scolecite, so more complex models may be required to simulate its hydration \emph{and} dehydration.

\subsubsection{Interatomic potentials elsewhere}
\label{interatomic_potentials_general}

\oiface{STILL NEEDS UN-BULLET-ING}

\begin{itemize}
    \item Laumontite and ``leonhardite''
    \begin{itemize}
        \item Interesting phenomenon: pressure-induced hydration~\cite{White2004}
        \item Suggested~\cite{Neuhoff2001_LAU} that the ambient form of laumontite is actually ``leonhardite'', with 14 \ce{H_{2}O} molecules per unit cell, and that fully-hydrated laumontite only forms at increased pressures
        \item IP models of LAU~\cite{White2004} agreed well with experiment~\cite{Fridriksson2003}, when used with \texttt{dehydrate} to investigate dehydration
        \item Water loss happens first from the W1 site, and then from the W5 site~\cite{Stahl1996}
        \item Preferred intermediate hydrated state with 16 \ce{H_{2}O} molecules~\cite{White2004}, wherein the \emph{average} enthalpy of hydration is lower for the first 4 water losses than it is for those remaining
    \end{itemize}
    \item Goosecreekite
\end{itemize}

\subsubsection{Density functional theory}
\label{dft}

% SHAMELESSLY SELF-PLAGIARISED FROM MSCI REPORT!

\oiface{The basis of density functional theory (DFT) involves the Born-Oppenheimer approximation, namely that nuclear motion is so slow in comparison to electronic motion that the nuclei in any system can be considered stationary, and thus also generating an effective static potential in which their surrounding electrons can move.  Under these assumed conditions, the Kohn-Sham\cite{Kohn1965} equations can be applied to the system, which adapt the general Schr\"{o}dinger equation to specifically describe a system of non-interacting particles (electrons): \begin{equation}\left(-\frac{\hbar^{2}}{2m}\nabla^{2} + v_{eff}(\mathbf{r})\right)\psi_{i}(r) = \varepsilon_{i}\psi_{i}(r).\end{equation}}

\oiface{A single stationary electronic state can therefore be described by a wavefunction $\psi$, given in terms in terms of electron density.  The exact solution cannot be found exactly for systems with more than one electron, but can be found to very high levels of precision using modern computational resources and approximations.}

\oiface{NEED TO REWORD TWO PARAGRAPHS ABOUT (TAKEN FROM MSCI REPORT)}

Density functional theory (DFT) methods provide ``basis sets'' to represent a given system, each made up of ``basis functions'', wherein each function represents one of the atomic orbitals $\phi_{i}$ in the system under consideration.  The linear combinations of these basis functions therefore compose the molecular orbitals.  Basis sets differ in their rigorousness, and thus also in their computational expense; for example, generalised gradient approximation (GGA) basis sets give more accurate models compared to local density approximation (LDA) basis sets, but take significantly longer to run.

DFT methods aim to minimise the energy functional given by \begin{equation}E[n] = T[n] + U[n] + \int{V(\mathbf{r})n(\mathbf{r})d^{3}r},\end{equation}in order to find the ground state electron density $n_{0}$ for a given system, in order to deduce the lowest-energy configuration.

Typically, DFT simulations iterate over a number of steps: the initial density $n$ is guessed, the atomic orbitals $\phi_{i}$ are computed using the Kohn-Sham\cite{Kohn1965} equations, and this ``density guess'' is refined accordingly; this process is repeated until a solution is reached within an acceptable accuracy ``tolerance''.

\oiface{UN-BULLET BELOW PLEASE!}

\begin{itemize}
    \item Many zeolites have been studied using DFT, with good results \textbf{REF}
    \item Those of particular interest for the current project are those most similar in structure or properties to the natrolite family, e.g.:
    \begin{itemize}
        \item Those on the denser end of microporosity, \textbf{SUCH AS...}
        \item Those that also undergo pressure-induced hydration (I had this all written out but \emph{where has it buggered off to, damn it all?}), \textbf{SUCH AS...}
    \end{itemize}
\end{itemize}

\subsubsection{Application of density functional theory to zeolite studies}
\label{dft_zeolites}

Density functional theory has successfully been applied to several zeolites in recent years~\cite{Nicholas1997}.  Zeolites studied include silica sodalite~\cite{Astala2004}, chabazite~\cite{Astala2004}, mordenite~\cite{Astala2004}, silica LTA~\cite{Astala2004}, silicalite~\cite{Astala2004}, zeolite Y~\cite{Niwa2013}, MFI-type zeolite~\cite{Stueckenschneider2012}, DOH-type zeolite~\cite{Smykowski2013}, and zeolite Ca-A~\cite{Arean2009}.

The natrolite family has also been the subject of DFT-based investigations~\cite{Mistry2005_THESIS,Kremleva2013,Kremleva2014,Liu2014,Fischer2015,Hwang2015_Cs-NAT,Hwang2015_PIAmorphisation}; this investigation hopes to contribute towards this existing body of knowledge.
