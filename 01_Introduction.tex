\section{Introduction}
\label{introduction}

\subsection{What are zeolites?}
\label{what_are_zeolites}

Zeolites are microporous aluminosilicate minerals, with three-dimensional open framework structures, and uniformly-sized pores with diameters of molecular dimensions, up to 20\r{A}.  These properties enable them to act as ``molecular sieves'', and they are thus commercially significant, particularly in areas where selectivity is difficult under typical reaction conditions.

\oiface{EXTENSION REQUIRED}

Their frameworks are composed of corner-linked TO$_{4}$ tetrahedra, where T is either (tetravalent) silicon or, typically, (trivalent) aluminium, wherein oxygen atoms act as ``bridges'', shared between adjacent tetrahedra.  The substitution of aluminium, or in some cases other non-tetravalent cations, in place of silicon creates a charge imbalance, with a distribution of negative charge accumulating around the aluminium; zeolites therefore usually require the addition of extra-framework cations (EFCs) --- most commonly protons, forming Br\o nsted acid sites~\cite{Weitkamp2000}, or Group 1 (or 2) metal ions --- to neutralise this imbalance.  These EFCs are loosely held within the frameworks, and so can be exchanged with other cations with comparative ease.  This allows for a wider variety of interactions between the zeolite and incoming external molecules, which makes them more useful in such areas as water treatment~\cite{Almalih2015} and petrochemical ``cracking'' processes~\cite{Yang2003_BOOK}.  Non-aluminium substituents have also been used, but this is often reserved for more specific applications; for example, the introduction of certain transition metal atoms allows finer tuning of the respective zeolite's catalytic properties~\cite{Schoonheydt1993,Corma2003,Godelitsas2003,Smeets2010}.

% $[SiO_{4}]^{4-}$ and $[AlO_{4}]^{5-}$

Zeolites are a naturally-metastable morphological form, while their denser, non-porous counterparts are the most stable form under ambient conditions.  They retain their metastable structures during formation due to the environmental conditions under which they were originally formed; natural zeolites are formed as a result of the reaction between silica-containing volcanic ash and groundwater, while synthetic zeolites are often formed using sol-gel processing under controlled conditions, thus allowing far finer control over the structure and properties of the final zeolitic product.

%\oiface{WHERE FOUND AND HOW FORMED IN NATURE} \textsuperscript{\oiface{REF}}

% Dempsey's and Loewenstein's rules

All zeolitic frameworks are composed of primary and secondary building units, the primary ones being the \ce{TO_{4}} tetrahedra, and the secondary ones typically taking one of several possible forms.  The geometry of the secondary building units, along wth their positioning relative to one another, dictates the structures of the zeolite's channels and pores; additionally, Loewenstein's and Dempsey's rules guide the possible framework configurations a zeolite is likely to form.

Loewenstein's rule~\cite{Loewenstein1954} states that any given zeolite framework cannot have two adjacent aluminium atoms bridged by an oxygen atom, and so the maximum Si:Al ratio for any zeolite is 1:1, wherein every \ce{[AlO_{4}]} tetrahedron is connected via O bridges to no fewer than four \ce{[SiO_{4}]} tetrahedra.  Zeolites with this maximum ratio, particularly when coupled with compensating acidic protons in their frameworks, are particularly well-suited to applications utilising their ion-exchange properties and \ce{[Al^{3+}-O^{-}-Si^{4+}][H^{+}]} Br\o nsted acid sites~\cite[pp.~81-197]{CatalysisAndZeolites}.

Dempsey's rule~\cite[p.~293]{Dempsey1968} states that Al-Al interactions in zeolite frameworks will aim to maximise the distance between aluminium atoms, where possible.

%\oiface{INSERT SOME KIND OF CONCLUSION ABOUT THE USE OF THESE RULES TO AID IN DEDUCING ZEOLITE STRUCTURES}
These rules --- or rather, guidelines --- can be paired with experimental techniques to relatively successfully deduce a zeolite's structural composition.  It has allowed over 200 distinct zeolite topologies to be discovered, characterised, and analysed~\cite{IZA_Database,Gaines1998_DANA}.

\subsubsection{Zeolite characterisation methods}
\label{characterisation_methods}

Many techniques can be used to elicit specific details about zeolite structures.  For example, X-ray diffraction (XRD) can yield useful information about the extra-framework cation sites, but tells us very little about the framework atoms~\cite{Almora-Barrios2001}.  Nuclear magnetic resonance (NMR) techniques can be used to provide very short-range details of a zeolite's Si/Al distribution~\cite{Zhao2001}, but gives no information about the framework longer in range than the first few bonds~\cite{Almora-Barrios2001}.  Thermal gravimetric analysis (TGA) gives details about preferred structures at the temperature(s) at which it is performed, but shows nothing about the mechanism of atomic rearrangement between preferred configurations over the continuous range of temperatures between those two temperatures at which they were tested.

A combination of methods, such as those above, can be used quite effectively to collectively deduce a zeolite's structure, alongside mindfulness of Dempsey's and Loewenstein's rules for framework composition.  However, for dynamic or mechanistic studies, such as hydration and dehydration, the available experimental techniques and the information they yield leave a lot to be desired; computational techniques can therefore compensate in those investigations where experimental techniques fall short.

\oiface{SHOULD I REFERENCE USEFULNESS OF COMPUTATIONAL TECHNIQUES IN HERE TOO?}

% Applications (zeolites generally)
\subsubsection{Applications}
\label{zeolite_applications}

% Depending on the amount of Si-Al substitution, and the nature of the extra-framework cations incorporated within the zeolitic pores, ion-exchange may be a viable marketable option, in which case the applications of such materials in industry are numerable.

The possibilities for commercial application of zeolites largely depends on their Si/Al ratio, the order in the Si/Al distribution, the nature and positioning of the extra-framework cations within the pores, and of course the dimensions of the zeolitic pores and channels.  Zeolites' inherent tunability --- in terms of acidity, reactivity, and stability, for example --- capacity to undergo ion-exchange, and ability to withstand certain extremes in experimental conditions, are the most valuable properties for industrial applications.

Zeolites not only provide greater overall surface areas, over which reactions may take place, than their denser solid-state analogues, making them particularly useful as adsorbents in catalysis, but they also enable reactions to take place selectively within their channels and pores, depending on their respective dimensions and steric compatibility~\cite{Kaeding1981_Part2,Young1982_Part3,Kaeding1984_Part4,Kaeding1985_Part5,Kaeding1988_Part6,Kaeding1989_Part7,Chen1996}.  Synthetic zeolites are also tunable, in terms of their design and creation, since they are ``grown'' around organic templates, which themselves can be tailored and synthesised for the purpose.

The aluminium-rich subset of zeolites, such as ZSM-5~\cite{Burger2000}, with their higher proportions of charge-compensating sites, are useful as sorbents~\cite{Vjunov2014} and as catalysts for specific kinds of reaction~\cite{Machado1999}.

The more silicon-rich distributions~\cite{Martens1987} lend themselves well to catalytic applications, as their distinct ion-exchange sites can be used more selectively, in contrast to the more generalised ion-exchange properties of aluminium-rich zeolites; the stability provided by a high Si/Al ratio enables such zeolites to withstand higher temperatures, such as those often required in catalytic processes~\cite[p.~2]{ZeoliteScienceAndPractice}.

The uniformity of zeolitic pores allow for their use in shape-selective reactions; the pores and channels of the zeolite impose a steric constraint on the reactions taking place within them.  They can, therefore, guide such reactions towards better yields of the desired product~\cite[pp.~327-370]{CatalysisAndZeolites}~\cite{Schenk2001}.  Zeolites are well-known commercially for their use in the petrochemical and oil refinement industries, for example in crude oil ``cracking'' processes~\cite{Yang2003_BOOK}, the premise of which involves the ``splitting'' of long-chained hydrocarbons into shorter-chained ones, and so within the zeolite only those shorter than a specific length --- determined by the diameter of the zeolitic pores and channels --- are free to pass through, whereas those exceeding said length, before or after reacting, are either retained in the pores indefinitely or until they have reacted and ``split'' further.  Similar reasoning enables zeolites' use in gas-separation reactions~\cite{Ackley2003}, where the gases to be separated are clearly distinguishable by size.

They have also found use in laundry detergents~\cite{Cutler1987,Sekhon2004} --- undergoing ion-exchange to remove the ``hard'' Group 2 cations from the washing water and replace them with ``softer'' Group 1 cations, thus reducing the ``dulling'' effect of the harder cations on the laundered fabrics --- and in horticulture and agriculture, due to their soil-conditioning potential, as they can aid in the retention of nitrogen, potassium, and water in the soil, reducing the need for soil ``refreshment''~\cite{Valente1982}, and for the treatment of manure to make the best use of its ammonia~\cite{Ghan2005}, enabling it to be used more efficiently as fertiliser~\cite{Nguyen1998}.

One important area of interest for this study specifically is that of zeolitic storage applications.  Zeolites are already in widespread use in wastewater treatment~\cite{Nguyen1998}, as they can effectively neutralise the harmful components of the wastewater by trapping them within their pores; they have also been utilised as nuclear waste storage media~\cite{Mercer1978,Dyer1984}, which is particularly relevant given current industrial demands for more sustainable alternatives to fossil fuels.  This makes use of those zeolites which expand under pressure, enabling the nuclear waste to be inserted into their pores, and around which the zeolite contracts when the pressure is removed.

% Natrolite family introduction

\subsection{The natrolite family}
\label{natrolite_family}

The natrolite family will be the main focus of the first stages of this investigation.  Natrolite, scolecite, and mesolite all share the NAT topology, with Si/Al ratios of 1.5, and are of particular interest because they have ordered Al distributions within the silicate frameworks, making them comparatively easy to model and analyse.  They also undergo interesting high-pressure- and high-temperature-induced phenomena: pressure-induced hydration~\cite{Lee2001,Lee2002_PIExpansion,Lee2002_K-GaSi-NAT,Lee2006,Lee2010,Lee2013,Seoung2013,Seoung2015} and negative thermal expansion~\cite{Lee2001}, respectively, which are important properties requiring further study, since they have proven useful in storage-based applications.

% CF: Other zeolites that undergo PIH include zeolite type A~\cite{Secco2000}, laumontite~\cite{Fridriksson2003,White2004,Rashchenko2012}, gismondine~\cite{Lee2008_K-GaSi-GIS,Orri2008}, FER-type zeolites~\cite{Arletti2014}, MFI-type zeolites~\cite{Vezzalini2014}, boggsite~\cite{Arletti2010}, thomsonite~\cite{Likhacheva2007}, mordenite~\cite{Arletti2015}

% General/review papers including NTE include~\cite{Lightfoot2001,Baur2003,Neuhoff2004,Liu2011,Gatta2014}

\oiface{MINI-VERSION OF STRUCTURAL DETAILS?}

Natrolite is currently used in the water-purification and chemical-filtering industries, capitalising on its ion-exchange properties~\cite{Barrer1959,Margeta2013}.  Scolecite is particularly useful in wastewater decontamination for the same reason~\cite{DalBosco2005,Wang2010}.  Mesolite has been utilised as storage for tritiated water~\cite{Faghihian1998}, and is thus also promising as a storage medium for radioactive waste~\cite{Sharma2008}.  It is the development of possibilities in this area that this investigation is well-placed to consider from a computational standpoint, first by focusing on modelling the fully-hydrated natrolite family as accurately as possible, and then by simulating the incorporation of various components of nuclear waste under different conditions --- particularly those extreme conditions that are hard to produce experimentally, and which are the most pertinent for safety-related studies of these applications.

% Other minerals of interest (in the near future, anyway)

\subsection{Other minerals of interest}
\label{other_minerals_of_interest}

The natrolite family is not the only set of relevant minerals under consideration in this investigation.  Mutinaite is a natural analogue of ZSM-5, which is orthorhombic with a high thermal stability and random Si/Al distribution~\cite{Galli1997,Vezzalini1997}.  It is of interest for similar commercial purposes as the natrolite family, and so deserves a mention for future stages of this investigation; its formula is given by \ce{Na_{3}Ca_{4}Si_{85}Al_{11}O_{192}$\cdot$ 60 H_{2}O}.
\oiface{INSERT SHINIER WORDS HERE}

Boggsite~\cite{Howard1990,Pluth1990} is another potential zeolite of interest, which also undergoes pressure-induced superhydration~\cite{Arletti2010}; at the time of writing, relatively little is known about it, but it has recently been produced experimentally~\cite{Simancas2010}.  Its formula is given by \ce{NaCa_{2}(Al_{5}Si_{19}O_{48})$\cdot$ 17 H_{2}O}.
\oiface{INSERT SHINIER WORDS HERE}

Chabazite has a random Si/Al distribution~\cite{Dent1958} and undergoes negative thermal expansion~\cite{Woodcock1999}, and so may be useful as a subject of comparison, in terms of both the criteria with which they must be modelled and potentially also their applications, against the (ordered) natrolites.  Its formula is given by \ce{(Ca,Na_{2},K_{2},Mg)Al_{2}Si_{4}O12$\cdot$ 6 H_{2}O}.
\oiface{INSERT SHINIER WORDS HERE}

% \subsection{Research importance}
% \label{research_importance}

\subsubsection{Effects of temperature and pressure}
\label{temperature_pressure_importance}

The nature of zeolites' three-dimensional open frameworks widen the scope for temperature- and pressure-induced structural distortions.  Phenomena such as negative thermal expansion~\cite{Lightfoot2001} and pressure-induced hydration~\cite{Lee2001} are fairly common amongst microporous materials~\cite{Lightfoot2001}.

\oiface{INSERT MORE WORDS HERE!}

\subsubsection{Natrolites}
\label{natrolites_importance}
\oiface{THIS WHOLE SECTION NEEDS EXPANDING}

The natrolite family of zeolites are comparatively dense for microporous materials~\cite{Lee2002_PIExpansion,Baur2003}, and it is therefore more difficult to obtain experimental details of their inner structures.

They also undergo pressure- and temperature-induced structural changes~\cite{Lee2001}, which may give us more insight into the wider potential for their usage industrially.

They are particularly applicable for the future development of industrial usage as nuclear waste storage materials~\cite{Kazemian2001}.

\subsection{Research questions}
\label{research_questions}

\subsubsection{Modelling zeolitic temperature and pressure effects}
\label{modelling_temperature_pressure_effects}

Since the conditions in which a zeolite is formed are instrumental in their structure, stability, and properties, it is unsurprising that changes in these conditions will alter the chemistry of the zeolite in question.  However, it is not always viable to test the effect of changes in these conditions on a zeolite experimentally, particularly when the extremes of pressure and temperature that may be important for investigating potential applications of the zeolite are either costly or dangerous to recreate.

Computational simulations can offer a much easier route to exploration of these environmental effects without the practical limitations of more traditional techniques, but they are not without their own limits.  As would be expected, as the size of a system being simulated increases, so does the runtime of the simulation and the computational power required to run it; likewise, the more accurate the model used to describe the system, the more those same limitations apply.

Much of computational research is dependent on the optimal balance between model accuracy and computational expense, which can often rely on a trial-and-error approach to find; the main modelling-related question in this investigation asks where this balance lies in regards to zeolites, and how best to achieve it.

Therefore, this investigation aims to answer the question: how well can zeolites, under extremes in thermodynamic conditions, such as temperature and pressure, be modelled, and does the accuracy of the model depend on the order of the Si/Al distribution, number and/or type of extra-framework cations, or hydration state of the zeolite; if so, how?  A logical question to follow this would be: which models --- and their associated parameters --- have the least complexity or computational intensity, but retain enough accuracy to fully reproduce experimental observations for the natrolite family of zeolites?

% Hydration states
\subsubsection{Zeolitic hydration states}
\label{hydration_states_importance}

Geologically, the hydration states of zeolites are of particular importance because zeolites are hydrated in their natural form; thus, modelling them in dehydrated form is of little use for real-world purposes, when such forms are rarely formed naturally.

The presence, and amount, of water in zeolitic pores has been shown to influence the distribution of aluminium in the silicate frameworks~\cite{Almora-Barrios2001}, and also the positioning of the charge-compensating cations, in relation to the incorporated water molecules, which provide both electrostatic and steric shielding between the cations and frameworks.

This investigation is particularly focused on the effects of pressure and temperature on the hydration states of zeolites, since the current specialist disposal sites using zeolites as nuclear waste storage are subject to environmental changes; the security of the nuclear waste within the storage materials and the safety of those handling them are of paramount importance, and so this area requires further study, in order to prevent the possibility of environmental contamination from the unanticipated structural changes in nuclear waste storage materials.  Therefore, a further question to be investigated is whether the long-term stability of zeolites adversely affected by consistently high pressures and temperatures, and/or the frequent changing in these conditions?

% Given the typical environmental conditions found in nuclear waste storage depositories, at what point does changing these conditions cause the safety of the nuclear waste storage materials to be compromised?

% Mesolite thermodynamics
\subsubsection{The thermodynamics of dehydration of mesolite}
\label{mesolite_thermodynamics}

Experimentally, it has been found that mesolite dehydrates by losing the calcium-coordinated water molecules first, but this is reversed in the computational predictions for the dehydration of mesolite using interatomic-potential models~\cite{Gibbs2004}, as compared to experimental findings~\cite{Stahl1994_XRD_Mes}.  The strength of binding between calcium and water and sodium and water, respectively, have evidently not been accurately constructed, given the same parameters as natrolite and scolecite.

A further aim of this investigation would be to ascertain whether the issue causing this is with the type of model being used, the parameters within the model, or the currently-accepted structure --- deduced from crystallographic data --- on which the input file coordinates are based.

AND WE AIM TO TEST ALL THE THINGS USING GULP AND CP2K

% Code-y stuff and database-building endeavours
\subsubsection{Code and databases}
\label{code_databases}
\oiface{INSERT DETAILS OF SOFTWARE-WHISPERING AND STRUCTURAL-DATABASE-ENORMOUS...ISING}

% Usefulness of temperature and pressure effects on applications of zeolites as nuclear waste storage media
\subsubsection{Structural distortions and storage applications}
\label{temperature_pressure_storage_applications}

\oiface{INSERT DETAILS OF T- AND P-INDUCED STRUCTURE-SQUIDGING'S USEFULNESS AS STORAGE MEDIUM}

Ideally, \oiface{FINISH MEEEE!}

\subsection{Hypotheses}
\label{hypotheses}

% How well can zeolites, under extremes in thermodynamic conditions, such as temperature and pressure, be modelled, and does the accuracy of the model depend on the order of the Si/Al distribution, number and/or type of extra-framework cations, or hydration state of the zeolite; if so, how?
The prediction for the modelling of zeolites under thermodynamic extremes, based on the results of previous investigations, is that most zeolites can be accurately modelled using computational models.  It is also reasonable to expect the models to be less accurate for less ordered Si/Al distributions, as the intrinsic distortion in the frameworks would certainly complicate the internal interactions.  Likewise, the extra-framework species incorporated into the zeolitic pores and channels are likely to affect the accuracy of the models, depending on whether or not the models have been created with their inclusion in mind.

% Which models --- and their associated parameters --- have the least complexity or computational intensity, but retain enough accuracy to fully reproduce experimental observations for the natrolite family of zeolites?
While interatomic potentials are mostly well-suited to modelling zeolites --- particularly those with ordered Si/Al distributions --- it is likely that some of the simpler electronic structure models will better represent the full range of zeolitic structures and experimentally-observed properties, thermodynamics included.

% Is the long-term stability of zeolites adversely affected by consistently high pressures and temperatures, and/or the frequent changing in these conditions?

% Is the issue causing the reversed dehydration thermodynamics of mesolite to do with the \emph{type} of model being used, the parameters \emph{within} the model, or the currently-accepted structure --- deduced from crystallographic data --- on which the input file coordinates are based?
